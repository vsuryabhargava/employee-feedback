import * as React from "react";
import { IFeedbackForm } from "../../App.types";
import { InputField } from "../InputField/InputField";
import "./styles.css";

export const FeedbackFrom: React.FC<{
  feedbackFrom: IFeedbackForm;
  readonly: boolean;
  isManagerView?: boolean;
  onSaveDraft?: (draft: IFeedbackForm) => void;
  onSubmit?: (form: IFeedbackForm) => void;
}> = React.memo(
  ({ feedbackFrom, onSaveDraft, onSubmit, readonly, isManagerView }) => {
    // initialize form values
    const [achievements, setAchievements] = React.useState<
      IFeedbackForm["formData"]["achievements"]
    >(feedbackFrom.formData.achievements);

    const [comments, setComments] = React.useState<
      IFeedbackForm["formData"]["comments"]
    >(feedbackFrom.formData.comments);

    const [improvements, setImprovements] = React.useState<
      IFeedbackForm["formData"]["improvements"]
    >(feedbackFrom.formData.improvements);

    /**
     * This triggers the save draft functionality
     * automatically after a 3 sec delay when the
     * relevant fields have changed
     */
    React.useEffect(() => {
      if (readonly || !onSaveDraft) {
        return;
      }
      const timeout = setTimeout(() => {
        onSaveDraft({
          ...feedbackFrom,
          formData: {
            achievements,
            comments,
            improvements,
          },
        });
      }, 300);
      return () => clearTimeout(timeout);
    }, [feedbackFrom, onSaveDraft, achievements, comments, improvements]);

    // on submit event handler
    const onSubmitEventHandler = (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      if (readonly || !onSubmit) {
        return;
      }
      onSubmit({
        ...feedbackFrom,
        formData: {
          achievements,
          comments,
          improvements,
        },
      });
    };

    // on discard event handler
    const onDiscardEventHandler = React.useCallback(
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
        if (readonly) {
          return;
        }
        setAchievements("");
        setComments("");
        setImprovements("");
      },
      [onSubmit, readonly]
    );

    const managerString = isManagerView ? "I" : "your manager";

    return (
      <form className="FeedbackForm" onSubmit={onSubmitEventHandler}>
        {!readonly && (
          <div>
            Hi <b>{feedbackFrom.user.username}</b>, please enter your feedback
            below
          </div>
        )}
        <InputField
          value={achievements}
          title={`What should ${managerString} continue doing?`}
          required
          onChange={setAchievements}
          readonly={readonly}
        />
        <InputField
          value={improvements}
          title={`What should ${managerString} consider improving?`}
          required
          onChange={setImprovements}
          readonly={readonly}
        />
        <InputField
          value={comments}
          title={"Other comments?"}
          required
          onChange={setComments}
          readonly={readonly}
        />
        <div>
          <div className="FormActions">
            <input disabled={readonly} className="SubmitButton" type="submit" />
            <input
              disabled={readonly}
              type="button"
              onClick={onDiscardEventHandler}
              value="discard"
            />
          </div>
        </div>
      </form>
    );
  }
);

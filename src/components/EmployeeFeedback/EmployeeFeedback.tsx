/* eslint-disable @typescript-eslint/no-floating-promises */
import * as React from "react";
import { IFeedbackForm, IUser } from "../../App.types";
import { getFromData, submitFromData } from "../../Utils/ApiUtils";
import { isValidFormSubmissionTime } from "../../Utils/dateUtils";
import {
  getDraftFeedbackData,
  saveFeedbackDraft,
} from "../../Utils/localStoreUtils";
import { FeedbackFrom } from "../FeedbackFrom/FeedbackForm";
import "./styles.css";

export const EmployeeFeedback: React.FC<{
  user: IUser;
  isManagerView?: boolean;
}> = React.memo(({ user, isManagerView }) => {
  // initialize form
  const [form, setForm] = React.useState<IFeedbackForm>({
    formData: {
      achievements: "",
      comments: "",
      improvements: "",
    },
    user,
  });
  const [isSaving, setIsSaving] = React.useState<boolean>(false);
  const [submittingForm, setSubmittingForm] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [formSubmitted, setFormSubmitted] = React.useState<boolean>(false);
  const [error, setError] = React.useState<string>("");
  const [isFormActive, setIsFormActive] = React.useState(false);
  const [pollCount, setPollingCount] = React.useState(0);

  /**
   * This check if form is active
   * i.e. is today between 1st jan - 31st jan
   */
  React.useEffect(() => {
    const active = isValidFormSubmissionTime();
    setIsFormActive(active);
    if (!active) {
      setError("You are not allowed to submit this form at the moment.");
    }
    const timeout = setTimeout(() => {
      setPollingCount(pollCount + 1);
    }, 60000);
    return () => clearTimeout(timeout);
  }, [pollCount]);

  /**
   * This loads data form api if form was submitted
   * else this loads data from draft
   */
  React.useEffect(() => {
    setLoading(true);
    const formDataProm = getFromData(user.username)
      .then(async (data) => {
        if (data) {
          setFormSubmitted(true);
          setForm(data);
        } else {
          try {
            const data = await getDraftFeedbackData(user.username);
            if (data) {
              setForm(data);
            }
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
          } catch (e) {
            // do nothing
          }
        }
      })
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .catch((e) => {
        setError("Unable to load user data");
      });
    formDataProm.then(() => setLoading(false));
  }, []);

  /**
   * On submit handler
   */
  const onSubmit = React.useCallback(async (data: IFeedbackForm) => {
    if (!data.formData.achievements.trim()) {
      setError("Please enter what your manager did good");
      return;
    }
    if (!data.formData.comments.trim()) {
      setError("Please enter additional comments");
      return;
    }
    if (!data.formData.improvements.trim()) {
      setError("Please the things your manager can improve on");
      return;
    }
    setSubmittingForm(true);
    try {
      await submitFromData(data);
      setFormSubmitted(true);
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (e) {
      setError(e ?? "Error saving data");
    }
    setSubmittingForm(false);
  }, []);

  const onSave = React.useCallback(async (data: IFeedbackForm) => {
    setIsSaving(true);
    try {
      await saveFeedbackDraft(data);
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (e) {
      // do noting
    }
    setIsSaving(false);
  }, []);

  /**
   * this will clear the error automatically after 3 secs
   */
  React.useEffect(() => {
    if (error) {
      const timeout = setTimeout(() => {
        setError("");
      }, 3000);
      return () => clearTimeout(timeout);
    }
  }, [error]);

  if (loading) {
    return (
      <div className="EmployeeFeedback">
        <h3>Loading...</h3>
      </div>
    );
  }

  return (
    <div className="EmployeeFeedback">
      {isManagerView ? null : (
        <>
          {error && !formSubmitted ? (
            <div className="error">{error}</div>
          ) : null}
          {submittingForm ? <div>Submitting form...</div> : null}
          {formSubmitted ? (
            <div>You have successfully submitted feedback for current year</div>
          ) : null}
        </>
      )}
      <FeedbackFrom
        onSaveDraft={onSave}
        onSubmit={onSubmit}
        readonly={
          !isFormActive ||
          formSubmitted ||
          loading ||
          submittingForm ||
          Boolean(isManagerView)
        }
        feedbackFrom={form}
        isManagerView={isManagerView}
      />
      {isSaving ? "Saving..." : null}
    </div>
  );
});

/* eslint-disable @typescript-eslint/no-floating-promises */
import * as React from "react";
import { IUser } from "../../App.types";
import { loadUserList } from "../../Utils/ApiUtils";
import "./styles.css";

export const User: React.FC<{
  user: IUser;
  onSelectUser: (user: IUser) => void;
  isSelected: boolean;
}> = React.memo(({ onSelectUser, isSelected, user }) => {
  const onClick = React.useCallback(() => onSelectUser(user), [
    user,
    onSelectUser,
  ]);

  return (
    <div
      onClick={onClick}
      className={`User ${isSelected ? "SelectedUser" : ""}`}
    >
      {user.username}
    </div>
  );
});

export const SubmittedFeedbacks: React.FC<{
  loggedinUser: IUser;
  onSelectUser: (user: IUser) => void;
  selectedUser: IUser;
}> = React.memo(({ loggedinUser, onSelectUser, selectedUser }) => {
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState<string>("");
  const [userList, setUserList] = React.useState<Array<IUser>>([]);
  const [pollCount, setPollCount] = React.useState<number>(0);

  React.useEffect(() => {
    if (error) {
      const timeout = setTimeout(() => setError(""), 3000);
      return () => clearTimeout(timeout);
    }
  }, [error, setError]);

  const fetchResults = React.useCallback(async () => {
    setLoading(true);
    try {
      const userList = await loadUserList(loggedinUser);
      setUserList(userList);
    } catch (e) {
      if (!userList.length) {
        setError("Error fetching data");
      }
    }
    setLoading(false);
  }, [userList]);

  React.useEffect(() => {
    fetchResults();
    const timeout = setTimeout(() => setPollCount(pollCount + 1), 15000);
    return () => {
      clearTimeout(timeout);
    };
  }, [pollCount]);

  if (error) {
    return <div className="error Messages">{error}</div>;
  }

  if (userList.length === 0 && !loading) {
    return (
      <div className="Messages">
        None of the employees have submitted feedback
      </div>
    );
  }

  return (
    <>
      <div className="Messages">Users who submitted feedback</div>
      {loading ? <div className="Messages">Loading...</div> : null}
      <div className="Users">
        {userList.map((u) => (
          <User
            key={u.username}
            onSelectUser={onSelectUser}
            user={u}
            isSelected={u.username === selectedUser.username}
          />
        ))}
      </div>
    </>
  );
});

import * as React from "react";
import { IUser } from "../../App.types";
import { EmployeeFeedback } from "../EmployeeFeedback/EmployeeFeedback";
import { SubmittedFeedbacks } from "../SubmittedFeedbacks/SubmittedFeedbacks";
import "./styles.css";

export const LandingPage: React.FC<{
  loggedinUser: IUser;
}> = React.memo(({ loggedinUser }) => {
  const [user, setUser] = React.useState<IUser>(loggedinUser);

  return (
    <div className="LandingPage">
      {loggedinUser.isManager ? (
        <div className="LeftSection">
          <SubmittedFeedbacks
            loggedinUser={loggedinUser}
            selectedUser={user}
            onSelectUser={setUser}
          />
        </div>
      ) : null}
      <div className="RightSection">
        {user.isManager ? null : (
          <EmployeeFeedback
            user={user}
            key={user.username}
            isManagerView={loggedinUser.isManager}
          />
        )}
      </div>
    </div>
  );
});

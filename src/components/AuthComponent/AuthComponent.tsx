import * as React from "react";
import { IUser } from "../../App.types";
import { getUser, login, logout } from "../../Utils/ApiUtils";
import { getLocalUser } from "../../Utils/localStoreUtils";
import "./styles.css";

const EMAIL_SUFFIX = "@someCorp.com";

export const AuthComponent: React.FC<{
  onChangeUser: (user?: IUser) => void;
}> = React.memo(({ onChangeUser }) => {
  const [username, setUsername] = React.useState<IUser["username"]>("");
  const [user, setUser] = React.useState<IUser | undefined>();
  const [loading, setLoading] = React.useState<boolean>(false);

  const onChangeEventHandler = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setUsername(e.target.value.trim());
    },
    []
  );

  const onLogin = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (username.trim()) {
      setLoading(true);
      try {
        const newUser = await login(username);
        setUsername(newUser?.username ?? "");
        setUser(newUser);
      } catch (e) {
        /**
         * Handle error
         */
      }
      setLoading(false);
    }
  };

  const onLogout = React.useCallback(async () => {
    setLoading(true);
    try {
      await logout(username);
      setUser(undefined);
      setUsername("");
    } catch (e) {
      /**
       * Handle error
       */
    }
    setLoading(false);
  }, []);

  React.useEffect(() => {
    setLoading(true);
    getLocalUser()
      .then(getUser)
      .then((user) => {
        setUsername(user?.username || "");
        setUser(user);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  React.useEffect(() => {
    onChangeUser(user);
    setUsername("");
  }, [user, onChangeUser, setUsername]);

  return (
    <form onSubmit={onLogin}>
      {user ? (
        <div className={"Auth"}>
          <div>{`${user.username}${EMAIL_SUFFIX}`}</div>
          {loading ? (
            "loading..."
          ) : (
            <input
              className="AuthButton"
              type="button"
              onClick={onLogout}
              value="logout"
            />
          )}
        </div>
      ) : (
        <div className={"Auth"}>
          <input
            type="text"
            onChange={onChangeEventHandler}
            placeholder="enter username"
          />
          <div>{EMAIL_SUFFIX}</div>
          {loading ? (
            "loading..."
          ) : (
            <input
              className="AuthButton"
              disabled={!username.trim()}
              type="submit"
              value="Login"
            />
          )}
        </div>
      )}
    </form>
  );
});

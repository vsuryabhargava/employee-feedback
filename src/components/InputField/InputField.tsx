import * as React from "react";
import "./styles.css";

export const InputField: React.FC<{
  value: string;
  title: string;
  onChange: (newVal: string) => void;
  readonly?: boolean;
  required?: boolean;
  disabled?: boolean;
}> = React.memo(({ onChange, title, value, readonly, required, disabled }) => {
  const onTextChange = React.useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      onChange(e.target.value);
    },
    [onChange]
  );

  return (
    <div>
      <h3 className={required ? "Required" : undefined}>{title}</h3>
      <textarea
        readOnly={readonly}
        disabled={disabled}
        name="w3review"
        rows={4}
        cols={50}
        value={value}
        onChange={onTextChange}
      />
    </div>
  );
});

/* eslint-disable no-debugger */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { rest } from "msw";
import { getYear, isValidFormSubmissionTime } from "../Utils/dateUtils";
import {
  submitFeedback,
  getSubmittedFeedback,
  getAllUsers,
} from "../Utils/dexieUtils";
import { isUserManager } from "../Utils/localStoreUtils";

/**
 * new Date('31 Dec 2020 18:30 UTC')
 * 1st jan 2021
 *
 * Test different times of server and client by changing the serverTime
 **/
// const serverTime = new Date("01 Jan 2020 18:30 UTC").toISOString();
const serverTime = new Date().toISOString();

const createUserObj = (username) => ({
  username,
  isManager: isUserManager(username),
});

export const handlers = [
  rest.post("/login", (req, res, ctx) => {
    const { username } = req.body;
    return res(ctx.status(200), ctx.json(createUserObj(username)));
  }),
  rest.get("/user/:username", (req, res, ctx) => {
    const { username } = req.params;
    return res(ctx.status(200), ctx.json(createUserObj(username)));
  }),
  rest.post("/postFeedback", async (req, res, ctx) => {
    // check if user is submitting in valid time
    const isValidTimePeriod = isValidFormSubmissionTime(serverTime);
    const submissionYear = req.body.year;
    const serverYear = getYear(serverTime);

    if (!isValidTimePeriod || submissionYear !== serverYear) {
      return res(
        ctx.status(400),
        ctx.json({
          error: `You can't submit feedback for ${submissionYear} right now`,
        })
      );
    }

    // check if user is trying to submit last years form
    const data = await submitFeedback(req.body);
    if (!data) {
      return res(
        ctx.status(400),
        ctx.json({
          error: "Not able to save data",
        })
      );
    }

    return res(ctx.status(200), ctx.json(data));
  }),
  rest.get("/feedback/:username/:year", async (req, res, ctx) => {
    const { username } = req.params;

    const year = parseInt(req.params.year, 10);

    const data = await getSubmittedFeedback(username, year);
    // instead of this I should have error codes which specify the type of error
    // if no such record exists then it should be handled better but no time to do this
    if (!data) {
      return res(
        ctx.status(200),
        ctx.json({
          formData: {
            achievements: "",
            comments: "",
            improvements: "",
          },
          user: createUserObj(username),
          isSubmitted: false,
        })
      );
    }

    return res(ctx.status(200), ctx.json({ ...data, isSubmitted: true }));
  }),
  rest.get("/feedbacks-for/:username/:year", async (req, res, ctx) => {
    const { username } = req.params;

    const year = parseInt(req.params.year, 10);

    if (!isUserManager(username)) {
      return res(ctx.status(401), ctx.json([]));
    }

    const data = await getAllUsers(year);
    if (!data) {
      return res(ctx.status(400), ctx.json(undefined));
    }

    return res(ctx.status(200), ctx.json(data));
  }),
];

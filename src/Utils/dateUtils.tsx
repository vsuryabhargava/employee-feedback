// doing it  badly because of the lack of time
// new Date('31 Dec 2020 18:30 UTC').getDate();
// new Date("31 Jan 2021 18:29:59 UTC"), // new Date('31 Jan 2021 18:29:59 UTC');

// specify now to simulate different times for server and client
export const isValidFormSubmissionTime = (now?: string): boolean => {
  const today = now ? new Date(now) : new Date();
  const month = today.getUTCMonth();
  // active only in jan of every year
  const active = 0 === month;

  return active;
};

export const getYear = (now?: string): number => {
  const today = now ? new Date(now) : new Date();
  return today.getUTCFullYear();
};

/* eslint-disable no-debugger */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import Dexie from "dexie";
import { IFeedbackForm, IUser } from "../App.types";

const DRAFT_FEEDBACK_DATABASE_NAME = "feedbacks";
const SUBMITTED_FEEDBACK_DATABASE_NAME = "submittedFeedbacks";
const SUBMITTED_USERS = "users";

const normalizeForm = (data: IFeedback): IFeedbackForm => ({
  formData: {
    achievements: data?.achievements ?? "",
    comments: data?.comments ?? "",
    improvements: data?.improvements ?? "",
  },
  user: {
    isManager: data?.isManager ?? false,
    username: data?.username ?? "",
  },
});

interface IFeedback {
  username: string;
  isManager: boolean;
  achievements: string;
  improvements: string;
  comments: string;
}

interface ISubmittedFeedback extends IFeedback {
  year: number;
}

class FeedbackDatabase extends Dexie {
  public [DRAFT_FEEDBACK_DATABASE_NAME]: Dexie.Table<IFeedback, string>;
  public [SUBMITTED_FEEDBACK_DATABASE_NAME]: Dexie.Table<
    ISubmittedFeedback,
    string
  >;
  public [SUBMITTED_USERS]: Dexie.Table<
    IUser & { id?: number; year: number },
    string
  >;

  public constructor() {
    super("FeedbackDatabase");
    this.version(1).stores({
      [DRAFT_FEEDBACK_DATABASE_NAME]:
        "username,isManager,achievements,improvements,comments,savedTimestamp",
      [SUBMITTED_FEEDBACK_DATABASE_NAME]:
        "[username+year],isManager,achievements,improvements,comments,savedTimestamp",
      [SUBMITTED_USERS]: "++id,username,isManager,year",
    });
    this[DRAFT_FEEDBACK_DATABASE_NAME] = this.table(
      DRAFT_FEEDBACK_DATABASE_NAME
    );
    this[SUBMITTED_FEEDBACK_DATABASE_NAME] = this.table(
      SUBMITTED_FEEDBACK_DATABASE_NAME
    );
    this[SUBMITTED_USERS] = this.table(SUBMITTED_USERS);
  }
}

const db = new FeedbackDatabase();

export const saveFeedbackLocally = async ({
  formData: { achievements, comments, improvements },
  user: { username, isManager },
}: IFeedbackForm) => {
  return await db[DRAFT_FEEDBACK_DATABASE_NAME].put(
    {
      achievements,
      comments,
      improvements,
      isManager,
      username,
    },
    username
  );
};

export const discardFeedbackDraft = (username: IUser["username"]) =>
  db[DRAFT_FEEDBACK_DATABASE_NAME].delete(username);

export const getFeedbackDraft = async (
  username: IUser["username"]
): Promise<IFeedbackForm | undefined> => {
  const data = await db[DRAFT_FEEDBACK_DATABASE_NAME].get(username);
  return data ? normalizeForm(data) : undefined;
};

export const getSubmittedFeedback = async (
  username: IUser["username"],
  year: number
): Promise<IFeedbackForm | undefined> => {
  const data = await db[SUBMITTED_FEEDBACK_DATABASE_NAME].where({
    year,
    username,
  }).first();
  return data ? normalizeForm(data) : undefined;
};

export const submitFeedback = async ({
  formData: { achievements, comments, improvements },
  user: { username, isManager },
  year,
}: IFeedbackForm & { year: number }) => {
  return db.transaction(
    "rw",
    db[SUBMITTED_USERS],
    db[SUBMITTED_FEEDBACK_DATABASE_NAME],
    async () => {
      await db[SUBMITTED_USERS].put({
        isManager,
        username,
        year,
      });
      return await db[SUBMITTED_FEEDBACK_DATABASE_NAME].put({
        achievements,
        comments,
        improvements,
        isManager,
        username,
        year,
      });
    }
  );
};

export const getAllUsers = async (year: number) => {
  return db[SUBMITTED_USERS].where({ year }).toArray();
};

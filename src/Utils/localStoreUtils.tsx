import { IFeedbackForm, IUser } from "../App.types";
import {
  discardFeedbackDraft,
  getFeedbackDraft,
  saveFeedbackLocally,
} from "./dexieUtils";

const MANAGER_NAME = "manager";
const USER_NAME_COOKIE = "USER_NAME_COOKIE";

function setCookie(
  cookieName: string,
  cookieValue: string,
  expiresInDays: number
) {
  const d = new Date();
  d.setTime(d.getTime() + expiresInDays * 24 * 60 * 60 * 1000);
  const expires = "expires=" + d.toUTCString();
  document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
}

function getCookie(cookieName: string) {
  const name = cookieName + "=";
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export const isUserManager = (username: string): boolean =>
  username.toLocaleLowerCase() === MANAGER_NAME;

export const getLocalUser = (): Promise<IUser["username"] | undefined> => {
  const username = getCookie(USER_NAME_COOKIE);
  return Promise.resolve(username);
};

export const setLocalUser = (user: IUser["username"]): Promise<void> => {
  setCookie(USER_NAME_COOKIE, user, 30);
  return Promise.resolve();
};

export const removeLocalUser = (): Promise<void> => {
  setCookie(USER_NAME_COOKIE, "", -1);
  return Promise.resolve();
};

/**
 * loads draft saved locally
 */
export const getDraftFeedbackData = async (
  username: IUser["username"]
): Promise<IFeedbackForm | undefined> => {
  const draft = await getFeedbackDraft(username);
  if (draft?.user.username) {
    return draft;
  }

  return Promise.resolve(undefined);
};

/**
 * saves drafts locally
 */
export const saveFeedbackDraft = async (form: IFeedbackForm): Promise<void> => {
  if (
    !form.formData.achievements &&
    !form.formData.comments &&
    !form.formData.improvements
  ) {
    return discardDraft(form.user.username);
  }

  await saveFeedbackLocally(form);
};

/**
 * saves drafts locally
 */
export const discardDraft = async (user: IUser["username"]): Promise<void> => {
  await discardFeedbackDraft(user);
  return;
};

/* eslint-disable @typescript-eslint/no-floating-promises */
import { IFeedbackForm, IUser } from "../App.types";
import { getYear } from "./dateUtils";
import { discardDraft, removeLocalUser, setLocalUser } from "./localStoreUtils";

async function postData<RequestType, ResponseType>(
  url: string,
  data: RequestType
) {
  const response = await fetch(url, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: JSON.stringify(data),
  });
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const respData = await response.json();
  if (response.status === 200) {
    return (respData as unknown) as ResponseType; // parses JSON response into native JavaScript objects
  } else {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    throw respData.error;
  }
}

async function getData<ResponseType>(url: string) {
  const response = await fetch(url);
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const respData = await response.json();
  if (response.status === 200) {
    return (respData as unknown) as ResponseType; // parses JSON response into native JavaScript objects
  } else {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    throw respData.error;
  }
}

export const getUser = async (
  username: IUser["username"] | undefined
): Promise<IUser | undefined> => {
  if (!username) {
    return undefined;
  }
  const user = await getData<IUser>(`/user/${username}`);

  setLocalUser(user.username);
  return user;
};

export const login = async (
  username: IUser["username"]
): Promise<IUser | undefined> => {
  const user = await postData<Pick<IUser, "username">, IUser>("/login", {
    username,
  });

  setLocalUser(user.username);
  return user;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const logout = (username: IUser["username"]): Promise<void> => {
  removeLocalUser();
  return Promise.resolve();
};

/**
 * Fetches data from backend
 */
export const getFromData = async (
  username: IUser["username"]
): Promise<IFeedbackForm | undefined> => {
  if (!username) {
    return undefined;
  }
  const form = await getData<IFeedbackForm & { isSubmitted: boolean }>(
    `/feedback/${username}/${getYear()}`
  );
  return form.isSubmitted ? form : undefined;
};

/**
 * saves data to backend
 */
export const submitFromData = async (form: IFeedbackForm): Promise<void> => {
  await postData<IFeedbackForm & { year: number }, IFeedbackForm>(
    "/postFeedback",
    { ...form, year: getYear() }
  );
  await discardDraft(form.user.username);
  return;
};

export const loadUserList = (user: IUser): Promise<Array<IUser>> => {
  return getData<Array<IUser>>(`/feedbacks-for/${user.username}/${getYear()}`);
};

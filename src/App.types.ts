export interface IFeedbackForm {
  user: IUser;
  formData: {
    achievements: string;
    improvements: string;
    comments: string;
  };
}

export interface IUser {
  username: string;
  isManager: boolean;
}

import React from "react";
import "./App.css";
import { AuthComponent } from "./components/AuthComponent/AuthComponent";
import { IUser } from "./App.types";
import { LandingPage } from "./components/LangingPage/LandingPage";

export const App: React.FC = () => {
  const [user, setUser] = React.useState<IUser | undefined>();

  return (
    <div className="App">
      <header className="App-header">
        <AuthComponent onChangeUser={setUser} />
      </header>
      {user ? (
        <LandingPage loggedinUser={user} />
      ) : (
        <h1>Please login to submit feedback</h1>
      )}
    </div>
  );
};

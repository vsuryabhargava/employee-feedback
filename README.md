# Employee manager feedback form

## How to run

1. npm i
2. npx msw init public/
3. npm start
4. open [http://localhost:3000](http://localhost:3000)

## Assumptions made

1. user can only submit only between 1st jan UTC (00:00:00) and 31st jan UTC (23:59:59) of any year (this needs to be checked at backend)
2. User can submit feedback of current year not previous and next years (this needs to be checked at backend)
3. You will see submitted form data and list of submission based on your current local time (changing the year on your machine will show you results of that year)
4. Manager users can't submit feedback
5. Manager username is `manager`

## About the repo

Used create react app with typescript to get the repo up and running
Added prettier so that code looks cleaner and gets formatted on save

### Functional tasks

1. ~~Basic UI~~
2. ~~Load and auto-save drafts of form~~
3. ~~Load data from api (to check if user already submitted data)~~
4. ~~Submit form~~
5. ~~Add validations (required etc)~~
6. ~~Check if form can be submitted (based on the time of the year)~~
   ^^ this should also be done in the `submit feedback` api in case the client changes the date
7. ~~Add login logout~~
8. ~~Add manager view~~
9. ~~Manager should see list of all submitted feedbacks~~ (preferably sorted by feedback submission time)
10. ~~Poll the list of submitted feedbacks (so that manager can see the new submissions without refreshing)~~
    ^^ this may not make sense with the current implementation but may be required in a real world use
11. ~~Users should still be logged in on refresh~~
12. Maybe virtualize the list of submissions because there may be a lot of them (or paginate results)

### UX tasks - not done

1. Show autosave icon when autosaving
2. Show better loading screen
3. Style all the components
4. maybe show shimmer for loading
5. Maybe add icons to each input field

### Future Enhancements - not done

1. Implement backend
2. show validation error for each field instead of a single error at the top
3. Create common hooks like usePoll useError instead of repeating the logic
4. Collate all constants in a single file
5. Can save submitted on as an ISOString instead of just saving the year
6. instead of sending code 200 when an error is there while fetching an existing form
   I should have error codes which specify the type of error
   if no such record exists then it should be handled better but no time to do this

## Retrospect

I shouldn't have used dexie (indexdb) when I have never used it before wasted quite a bit of time debugging issues :(
I used it thinking I'll paginate sort etc but not enough time
